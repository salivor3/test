<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $thumbnailUrl;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visiblePublic;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visibleAuthenticated;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getThumbnailUrl(): ?string
    {
        return $this->thumbnailUrl;
    }

    public function setThumbnailUrl(string $thumbnailUrl): self
    {
        $this->thumbnailUrl = $thumbnailUrl;

        return $this;
    }

    public function getVisiblePublic(): ?bool
    {
        return $this->visiblePublic;
    }

    public function setVisiblePublic(bool $visiblePublic): self
    {
        $this->visiblePublic = $visiblePublic;

        return $this;
    }

    public function getVisibleAuthenticated(): ?bool
    {
        return $this->visibleAuthenticated;
    }

    public function setVisibleAuthenticated(bool $visibleAuthenticated): self
    {
        $this->visibleAuthenticated = $visibleAuthenticated;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'price' => $this->getPrice(),
            'category' => $this->getCategory()->getLabel(),
            'thumbnailUrl' => $this->getThumbnailUrl(),
            'visible_public' => $this->getVisiblePublic(),
            'visible_authenticated' => $this->getVisibleAuthenticated()
        ];
    }
}
