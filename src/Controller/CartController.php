<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    private ProductRepository $productRepository;
    private CartRepository $cartRepository;
    private UserRepository $userRepository;

    /**
     * CartController constructor.
     */
    public function __construct(ProductRepository $productRepository, CartRepository $cartRepository, UserRepository $userRepository)
    {
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/cart/{userID}", name="getCart", methods={"GET"})
     */
    public function cart(int $userID): Response
    {
        $user = $this->userRepository->find($userID);

        if (empty($user)) {
            throw new NotFoundHttpException('The user does not exist');
        }

        $cart = $this->cartRepository->findOneBy(['user' => $user]);

        if (!$cart) {
            return $this->json([]);
        }

        $result = array_map(function($product) {
            return $product->toArray();
        }, $cart->getProducts()->toArray());

        return $this->json($result);
    }

    /**
     * @Route("/cart/{userID}/{productID}", name="addToCart", methods={"POST"})
     */
    public function addProduct(int $userID, int $productID): Response
    {
        $user = $this->userRepository->find($userID);

        if (empty($user)) {
            throw new NotFoundHttpException('The user does not exist');
        }

        $product = $this->productRepository->find($productID);

        if (empty($product)) {
            throw new NotFoundHttpException('The product does not exist');
        }

        $cart = $this->cartRepository->findOneBy(['user' => $user]);

        if (!$cart) {
            $cart = new Cart();
            $cart->setUser($user);
        }

        $cart->addProduct($product);

        $this->getDoctrine()->getManager()->persist($cart);
        $this->getDoctrine()->getManager()->flush();

        return $this->json('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/cart/{userID}/{productID}", name="removeToCart", methods={"DELETE"})
     */
    public function removeProduct(int $userID, int $productID): Response
    {
        $user = $this->userRepository->find($userID);

        if (empty($user)) {
            throw new NotFoundHttpException('The user does not exist');
        }

        $product = $this->productRepository->find($productID);

        if (empty($product)) {
            throw new NotFoundHttpException('The product does not exist');
        }

        $cart = $this->cartRepository->findOneBy(['user' => $user]);

        if (!$cart) {
            throw new NotFoundHttpException('This user doesn\'t have cart');
        }

        $cart->removeProduct($product);

        $this->getDoctrine()->getManager()->flush();

        return $this->json('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/cart/{userID}", name="removeCart", methods={"DELETE"})
     */
    public function remove(int $userID): Response
    {
        $user = $this->userRepository->find($userID);

        if (empty($user)) {
            throw new NotFoundHttpException('The user does not exist');
        }

        $cart = $this->cartRepository->findOneBy(['user' => $user]);

        if (!$cart) {
            throw new NotFoundHttpException('This user doesn\'t have cart');
        }

        $this->getDoctrine()->getManager()->remove($cart);
        $this->getDoctrine()->getManager()->flush();

        return $this->json('', Response::HTTP_NO_CONTENT);
    }
}
