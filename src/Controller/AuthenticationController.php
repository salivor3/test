<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AuthenticationController extends AbstractController
{
    private UserRepository $userRepository;
    private UserService $userService;

    /**
     * CatalogController constructor.
     */
    public function __construct(UserRepository $userRepository, UserService $userService)
    {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }

    /**
     * @Route("/login/{id}/{password}", name="login", methods={"POST"})
     */
    public function login(int $id, string $password): Response
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            throw new NotFoundHttpException('The user does not exist');
        }

        $isValidPassword = $this->userService->isValidPassword($user, $password);

        return $this->json(['authenticated' => $isValidPassword]);
    }
}
