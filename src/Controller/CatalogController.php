<?php


namespace App\Controller;


use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CatalogController extends AbstractController
{
    private ProductRepository $productRepository;

    /**
     * CatalogController constructor.
     */
    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/catalog", name="catalog", methods={"GET"})
     */
    public function catalog(): Response
    {
        $products = $this->productRepository->findAllAuthenticated();

        $result = array_map(function(Product $product) {
            return $product->toArray();
        }, $products);

        return $this->json($result);
    }

    /**
     * @Route("/product/{id}", name="product", methods={"GET"})
     */
    public function product(int $id): Response
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            throw new NotFoundHttpException('The product does not exist');
        }

        return $this->json($product->toArray());
    }
}