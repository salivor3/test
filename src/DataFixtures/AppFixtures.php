<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setIndex(10)
            ->setLabel('Chaussures')
            ->setDescription('Toutes les chaussures avec scratch ou lacets');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setIndex(20)
            ->setLabel('Vestes')
            ->setDescription('Toutes les vestes avec ou sans boutons de mancette');
        $manager->persist($category2);

        $product1 = new Product();
        $product1->setLabel('Escarpin du dimanche')
            ->setDescription('Toujours sur son 31')
            ->setCategory($category1)
            ->setThumbnailUrl('local/img/ok')
            ->setPrice(123)
            ->setVisiblePublic(false)
            ->setVisibleAuthenticated(true);
        $manager->persist($product1);

        $product2 = new Product();
        $product2->setLabel('Basket running')
            ->setDescription('Pas plus de 10km')
            ->setCategory($category1)
            ->setThumbnailUrl('local/img/tryharder')
            ->setPrice(46)
            ->setVisiblePublic(false)
            ->setVisibleAuthenticated(false);
        $manager->persist($product2);

        $product3 = new Product();
        $product3->setLabel('Smoking à la James Bond')
            ->setDescription('Pour les missions impossibles')
            ->setCategory($category2)
            ->setThumbnailUrl('local/img/astonmartin')
            ->setPrice(249)
            ->setVisiblePublic(false)
            ->setVisibleAuthenticated(true);
        $manager->persist($product3);

        $user1 = new User();
        $user1->setName('super master')
            ->setEmail('master@gmail.test')
            ->setPasswordHash('');
        $manager->persist($user1);

        $user2 = new User();
        $user2->setName('stagiaire')
            ->setEmail('stage@gmail.test')
            ->setPasswordHash('');
        $manager->persist($user2);

        $manager->flush();

        $user1->setPasswordHash(md5($user1->getId() . 'bonjour'));
        $user2->setPasswordHash(md5($user2->getId() . 'bonjour'));

        $manager->flush();
    }
}
