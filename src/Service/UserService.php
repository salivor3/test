<?php


namespace App\Service;


use App\Entity\User;

class UserService
{
    public function isValidPassword(User $user, $password): bool
    {
        return $user->getPasswordHash() === md5($user->getId() . $password);
    }
}