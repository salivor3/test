### Récupération du projet
`git clone https://gitlab.com/salivor3/test.git`
### Acces au projet
`cd test`
### Lancement du projet
`docker-compose up`
### Récupérer le container php-fpm
`docker ps`
### Accéder au container
`docker exec -it <containerId> sh`
### Mettre à jour de schéma de base de données
`bin/console doctrine:schema:update --force`
### Ajout du jeu de données en base
`bin/console doctrine:fixture:load`